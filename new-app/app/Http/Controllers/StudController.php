<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
class StudController extends Controller
{
    public function insertform()
{
	return view('insert');
}
public function insert(Request $request)
{
$name=$request->input('name');
$std=$request->std;
DB::insert('insert into student(name,std) values(?,?)',[$name,$std]);
echo "Insert successfully";
echo "<br>";
echo "To Insert More <a href='/insert'>Click here </a>";
echo "<br>";
echo "To check list of Users <a href='/showuser' > click here </a>";
}
public function show()
{
$users=	DB::select('select * from student');
return view('showuser',['user'=>$users]);
}
public function edit(Request $request,$id)
{
DB::update('update student set name= ? , std= ? where id= ?',[$request->name,$request->std,$id]);
echo "Updated Successfully.";
echo "<br>";
echo "<a href='/showuser'>Click here</a> to go back";
}
public function showedit($id)
{
$user=DB::select('select * from student where id=?',[$id]);
return view('studupdate',['user'=>$user]);
}
public function studdelete($id)
{
DB::delete('delete from student where id= ?',[$id]);
echo "Student Record Deleted Successfully.";
echo "<br>";
echo "<a href='/showuser'>Click here</a> to go back";
}
}
