<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('hello');
});
Route::get('insert', 'StudController@insertform');
Route::get('register',function(){
	return view('register');
});
Route::post('create','StudController@insert');
Route::post('/user/register',array('uses'=>'AdminController@postRegister'));
//Route::redirect('/user/register','/get/user');
Route::get('/get/user',array('uses'=>'AdminController@getRegister'));
Route::resources([
    'password' => 'PasswordController'
]
);
Route::get('/showuser','StudController@show');
Route::get('edit/{id}','StudController@showedit');
Route::post('edit/{id}','StudController@edit');
Route::get('delete/{id}','StudController@studdelete');
