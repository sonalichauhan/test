# Installation

Installation of Required Properties For Ubuntu 18.04 .
## Requirements
1.   PHP ( 7.2+ or LATEST VERSION )
2.   Nginx
3.   Adminer
4.  Mysql
5.   Git
6.   Phantomjs
7.   Casperjs
8.   Gearman
9.   Redis
10.  MongoDB (PHP Mongo Drivers)
11. Tesseract

## Get Started
```bash
sudo apt-get update
sudo apt-get upgrade
```
1.Install PHP .
    
  Install php and all other tools of php.
  (Latest version <replace 7.2 with Latest Version>)

```bash
sudo apt-get install php7.2-cgi php7.2-cli php7.2-common php7.2-curl
sudo apt-get install php7.2-gd php7.2-json php7.2-mbstring php7.2-mysql 
sudo apt-get install php7.2-odbc php7.2-sqlite3 php7.2-xml php7.2-dba
sudo apt-get install php7.2-fpm php7.2-xmlrpc php7.2-zip php7.2-soap
```
Edit File and change parameters
```bash
sudo nano /etc/php/7.2/fpm/php.ini
```
Comment "session.save_handler = files" and Add "session.save_handler = redis"
Comment "session.save_path = /var/lib/php/sessions" and Add "session.save_path = SERVER_HOST"

2.Install Nginx .
  

```bash
sudo apt-get install nginx
sudo apt-get install nginx-extras
sudo rm /etc/nginx/sites-enabled/default
```

Create default.conf in /etc/nginx/conf.d/
```bash
sudo nano /etc/nginx/conf.d/default.conf
```
Put this code in default.conf
```bash
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /var/www;

        server_name localhost;
        index index.php index.html index.htm;

        autoindex on;
        client_max_body_size 1000M;
        allow all;

        location ~ \.php$ {
                try_files $uri =500;
                #fastcgi_pass 127.0.0.1:9000;
                fastcgi_pass unix:/run/php/php7.2-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
        }

        location ~ /\.ht {
                deny all;
        }
        #location ~ \.(png|jpg)$ {
                #try_files $uri /testing/images/unknown.png;
                #error_page 404 /testing/images/unknown.png;
        #}
}
```

3.Install Mysql Client.
  

```bash
sudo apt-get install mysql-client
```
After Installing Mysql client , Secure your mysql connection and set the root passwords.
```bash
sudo mysql_secure_installation
```
From here, you can just press Y and then ENTER to accept the defaults for all the subsequent questions.

4.Install Adminer.
  

```bash
sudo mkdir /usr/share/adminer
sudo wget "http://www.adminer.org/latest.php" -O /usr/share/adminer/latest.php
sudo ln -s /usr/share/adminer/latest.php /usr/share/adminer/adminer.php
echo "Alias /adminer.php /usr/share/adminer/adminer.php" | sudo nano /etc/nginx/nginx.conf
sudo cp /usr/share/adminer/adminer.php  /var/www/adminer.php
```
5.Install Redis.
```bash
sudo apt-get install php-redis
sudo apt-get install redis-tools
```

6.Install Git.
```bash
sudo apt-get install git git-gui
```
7.Install MongoDb.
```bash
sudo apt-get install php-mongodb
```
8.Install Gearman Client.
```bash
sudo apt-get -y install wget unzip re2c libgearman-dev
sudo mkdir -p /tmp/install
cd /tmp/install
wget https://github.com/wcgallego/pecl-gearman/archive/master.zip
unzip master.zip
cd pecl-gearman-master
sudo apt install php7.2-dev
phpize
./configure
sudo make install
```
Create a geraman.ini file with
```bash
sudo nano /etc/php/7.2/mods-available/gearman.ini
write "extension=gearman.so" in the file.
```
```bash
sudo phpenmod -v ALL -s ALL gearman
sudo rm -rf /tmp/install/pecl-gearman-master
sudo rm /tmp/install/master.zip
```
Verify if module is really installed
```bash
php -m | grep gearman
```
9.Set Up an NFS Mount
```bash
sudo apt-get install nfs-common
sudo mkdir -p /nfs/
sudo mount PRIVATE_IP_HOST:/var/nfs/ /nfs/
```
To mount you have to configure Hosts /etc/exports/ file and Add the PRIVATE_IP_CLIENT to it.

/PATH_TO_MOUNT    PRIVATE_IP_CLIENT(rw,sync)


## Contributing
Any updates or changes are welcome. 